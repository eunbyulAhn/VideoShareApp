import express from "express";
import {signup, signin, googleAuth} from "../controllers/auth.js";

const router = express.Router()

// Ceate a user
router.post("/signup", signup)

// sigh in
router.post("/signin", signin)

//google auth
router.post("/google",googleAuth)



export default router;